/* 1. Programa que muestre unas determinadas líneas de un fichero de texto. Tanto el fichero como los números de línea (en orden creciente)
 serán pasados como parámetros en la ejecución. Ejemplo:

	java ej1 fichero.txt 3 7 10 */

import java.io.*;

public class ej1
{
	public static void main(String[] args) {
		if (args.length > 0) {
			BufferedReader br = null; FileReader fr=null; String s;int lineaFich=1;int sigLinea=1;
			try
			{
				fr = new FileReader(args[0]);
				br = new BufferedReader(fr);
				s = br.readLine();
				while ((s != null) && (sigLinea < args.length))
				{
					if (Integer.parseInt(args[sigLinea]) == lineaFich)
					{
						System.out.println(s);
						sigLinea++;
					}
/*					else
						System.out.println("a");*/
					s = br.readLine();
					lineaFich++;
				}
			}
			catch(FileNotFoundException e)
			{
				System.out.println("El fichero no existe");
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
			finally
			{
				try
				{
					if(br != null)	br.close();
					if (fr != null) fr.close();
				}
				catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
			}
			
		}
		else
			System.out.println("Forma de uso: java ej1 /ruta/al/fichero linea1 linea2 ..." );
	}
}