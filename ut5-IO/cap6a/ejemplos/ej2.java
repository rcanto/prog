import java.io.*;

public class ej2
{
	public static String leercadena(BufferedReader br)
	{
	  String cad="";
	
	  try{
		cad = br.readLine();
	  }catch (IOException e ) {
	      e.printStackTrace();
	  }
	  return cad;
	}
	
	public static void main(String args[]) throws IOException
	{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	  	String cad;
	  
	  	System.out.println("Este programa hace eco hasta que escribas para");
	  	do {
	  // le paso como parámetro el objeto BufferedReader para evitar tener que crear un objeto BufferedReader en cada 	llamada a la función
			cad = leercadena(br);
			System.out.println(cad);
		} while(!cad.equals("para"));
	}
}

// Modifica este programa modular para hacerlo monolítico (lo mismo, todo en main)
