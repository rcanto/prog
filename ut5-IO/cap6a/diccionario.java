import java.io.*;
import java.util.*;

class Traductor implements Serializable
{
	private String spanish="";
	private String english="";
	public Traductor(String s,String e)
	{
		spanish=s; english=e;
	}
	public void set(String s,String e)
	{
		spanish=s; english=e;
	}
	public String get_spanish() {	return spanish; }
	public String get_english() {	return english; }
}

public class diccionario 
{
	private static final int tam = 3;
	public static void main(String[] args) throws ClassNotFoundException
	{
		String e="",s="";
		Scanner ent = new Scanner(System.in);
		try
		{
			
				FileOutputStream fos=new FileOutputStream("traductor.dat");
				ObjectOutputStream oos=new ObjectOutputStream(fos);
			
			//Traductor t=new Traductor();
			Traductor t;
		
			// EMPIEZO PIDIENDO PALABRAS CON SU TRADUCCIÓN Y ESCRIBIÉNDOLAS A FICHERO
			for (int i=0;i<tam;i++)
			{
				System.out.println("Introduce una palabra en inglés:");
				e=ent.next();
				System.out.println("Introduce una palabra en español:");
				s=ent.next();
				//t.set(s,e);
				t=new Traductor(s,e);
				System.out.println(t.get_spanish()+"----->"+t.get_english());
					
				
				oos.writeObject(t);
			}
			if (oos != null)
			{
				oos.close(); fos.close();
			}
				System.out.println("¿Qué palabra en español quieres traducir al inglés?");
				s=ent.next();	
			
		// AHORA LEO DE FICHERO PARA BUSCAR LA PALABRA A TRADUCIR
			FileInputStream fis=new FileInputStream("traductor.dat");
			ObjectInputStream ois=new ObjectInputStream(fis);	
			while (true)
			{
				//System.out.println("a");
				//t=null;
				t = (Traductor)ois.readObject();
				System.out.println(t.get_spanish()+"----->"+t.get_english());
				System.out.print(s);
				System.out.print(t.get_spanish());
				
				if (s.equals(t.get_spanish()))
				{
					System.out.println("Traducción: " + t.get_english());
					break;
				}
			}
			if (ois != null)
			{	ois.close(); fis.close(); }
		}
		/* catch (FileNotFoundException e2)
		{	e2.getMessage(); } */
		catch (IOException e1)
		{ e1.getMessage(); }
		
		
		System.exit(0);
	}
}
