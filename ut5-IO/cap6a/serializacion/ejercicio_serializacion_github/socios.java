import java.util.*;
import java.io.*;

class socio implements Serializable
{
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String nif;
	private GregorianCalendar nacimiento;
	// constructor
	public socio(String n, String a1, String a2, String nif, GregorianCalendar c)
	{
		nombre=n; apellido1=a1; apellido2=a2; this.nif=nif; nacimiento=c;

	}
	public void muestra()
	{
		System.out.printf("\nNombre: %s\tDNI: %s\tFecha nacimiento:",nombre+" "+apellido1+" "+apellido2,nif); 
		System.out.println(nacimiento.get(Calendar.DAY_OF_MONTH)+"/"+(1+nacimiento.get(Calendar.MONTH))+"/"+nacimiento.get(Calendar.YEAR));		
	}
}

class socios
{
	public static void main(String[] args) throws IOException
	{
		// Crear un File para socios.txt
		// Crear un Scanner sobre FileReader
		File f=new File("socios.txt");
		Scanner sc=new Scanner(f);
		// leer, con nextLine, cada línea del fichero con un bucle
		// se serializará con ObjectOutputStream
		FileOutputStream fos=new FileOutputStream("socios.dat");
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		while(sc.hasNext())
		{
			String linea=sc.nextLine();
		// crear a partir de la línea el StringTokenizer
			StringTokenizer st=new StringTokenizer(linea);
		// cada token será un parámetro para el constructor de socio
			String nombre=st.nextToken();
			String apellido1=st.nextToken();
			String apellido2=st.nextToken();
			String nif=st.nextToken();
			String fecha=st.nextToken();
			StringTokenizer stfecha=new StringTokenizer(fecha,"/");
			int dia=Integer.parseInt(stfecha.nextToken());
			int mes=Integer.parseInt(stfecha.nextToken())-1;
			int anyo=Integer.parseInt(stfecha.nextToken());
			GregorianCalendar gc=new GregorianCalendar(anyo,mes,dia);
			socio s=new socio(nombre,apellido1,apellido2,nif,gc);
			// serializo el socio
			oos.writeObject(s);
		}
				// una vez acabado el bucle, cerraremos ambos ficheros
		if (oos!=null)
		{
			oos.close();
			fos.close();
		}
		if (sc!=null)
		{	sc.close(); //f.close();
		}
		
		// ahora "des-serializamos"
		FileInputStream fis=new FileInputStream("socios.dat");
		ObjectInputStream ois=new ObjectInputStream(fis);
		try
		{
			while (true)
			{
				//System.out.println("a");
				socio s=(socio)ois.readObject();
				s.muestra();
			}
		}
		catch (EOFException e)
		{
			System.out.println("Fin de fichero");
		}
		catch (ClassNotFoundException e)
		{
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		finally
		{
			if (ois != null)
			{	ois.close();fis.close();
				System.exit(0);
			}
		}		

	}	// fin main
}	// fin clase socios
