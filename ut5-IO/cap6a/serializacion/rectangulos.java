/* 
	https://stackoverflow.com/questions/46262974/serialization-appending-to-object-files-in-java
*/

import java.util.Scanner;
import java.io.*;
import java.nio.*;
import java.nio.file.*;

class AppendingObjectOutputStream extends ObjectOutputStream {

  public AppendingObjectOutputStream(OutputStream out) throws IOException {
    super(out);
  }

  @Override
  protected void writeStreamHeader() throws IOException {
    // do not write a header, but reset:
    // this line added after another question
    // showed a problem with the original
    reset();
  }

}

class rectangulo implements Serializable
{
    private double ancho,alto;
    public rectangulo(double an,double al) {ancho=an;alto=al;}
    public String toString() { return ancho + "," + alto;}
}

public class rectangulos
{
    public static void main(String args[]) throws ClassNotFoundException
    {
        Scanner ent = new Scanner(System.in);
        System.out.println("Introduce ancho");
        double an = ent.nextDouble();
        System.out.println("Introduce alto");
        double al = ent.nextDouble();
        rectangulo r = new rectangulo(an,al);
        try
        {
		/*File*/OutputStream fos = null;
		ObjectOutputStream oos = null;
		File f = new File("rect.dat");
		if (f.exists())
		{
			Path path = FileSystems.getDefault().getPath("rect.dat");
			fos = Files.newOutputStream(path, StandardOpenOption.APPEND);
			oos = new AppendingObjectOutputStream(fos);
		}
		else
		{
			fos = new FileOutputStream(f);
			oos = new ObjectOutputStream(fos);
		}
	        oos.writeObject(r);
	        oos.close();
	        FileInputStream fis = new FileInputStream("rect.dat");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        while(true)
	        {
	        	r = (rectangulo)(ois.readObject());
	        	System.out.println(r);
	        }
	    }
	    catch(EOFException e) { System.out.println("TheEnd");}
	    catch(IOException e) { System.err.println(e.getMessage());}
    }
}
