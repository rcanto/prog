// Programa que pida valores numéricos al usuario, hasta acabar con un cero, y los escriba en binario (8 Bytes, 64 bits) a fichero

import java.util.Scanner;
import java.io.*;

public class ejEscrituraLecturaBinaria
{
	public static void main(String args[])
	{
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		Scanner ent = new Scanner(System.in);

    // ESCRITURA
		System.out.println("Introduce un número (0 para acabar);");
		double num = ent.nextDouble();
		try
		{
			fos = new FileOutputStream("nums.dat",true);
			dos = new DataOutputStream(fos);
			while (num != 0)
			{
				dos.writeDouble(num);
				System.out.println("Introduce un número (0 para acabar);");
				num = ent.nextDouble();
			}
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
                if (dos != null)
                {
				    dos.close();
				    fos.close();
                }
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
    // LECTURA: LEO DEL MISMO FICHERO
        FileInputStream fis = null; 
        DataInputStream dis = null;
        try
        {
            fis = new FileInputStream("nums.dat");
            dis = new DataInputStream(fis);
            num = dis.readDouble();
            while (true)    // aparente bucle infinito, realmente saldrá con la EOFException
            {
                System.out.println(num);
                num = dis.readDouble();
            }
        }
        catch (EOFException e)
        {
            System.out.println("Fin del fichero");
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
		finally
		{
			try
			{
                if (dis != null)
                {
				    dis.close();
				    fis.close();
                }
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
	    }
}
