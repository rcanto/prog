// 7. Realiza un programa que elimine todos los espacios en blanco repetidos en un fichero de texto.
import java.io.*;

public class ej7
{
	public static void main(String[] args) {
		if (args.length > 0)
		{
			String linea;
			// creo el flujo de entrada o lectura
			File f = new File(args[0]);
			File f2=null;
			try
			{
				FileReader fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				// creo el flujo de salida o escritura
				f2 = new File(args[0]+".new");
				FileWriter fw = new FileWriter(f2);
				// leo línea a línea desde el flujo de entrada
				while((linea = br.readLine()) != null )
				{
					char l[] = linea.toCharArray();
					boolean esp=false;
					for ( int i=0 ; i < l.length ; i++)
					{
						
						if ( l[i] != ' ')
						{
							esp = false;
							fw.write(l[i]);
						}
						else	// es un espacio en blanco
							if (esp == false)
							{
								fw.write(l[i]);
								esp = true;
							}
							//else // es espacio repetido
					}
					fw.write('\n');
				}
				if (fw != null) fw.close();
				if (br != null) { br.close(); fr.close();}
			}
			catch (FileNotFoundException e)
			{
				System.err.println("Fichero no encontrado");
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
			f.delete();
			f2.renameTo(f);
		}
		else	// Si no se indica el nombre del fichero
			System.out.println("Forma de uso: ej7 nomFichero");
	}
}
