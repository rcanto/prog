import java.io.*;
import java.util.*;

class cuenta
{
	public int cuentaPalabras(File f) throws IOException
	{
		Scanner ent=new Scanner(f);
		int cont=0;
		
		while(ent.hasNext()==true)
		{
			ent.next(); //
			cont++;
		}
		return cont;
	}
}

public class ej6
{
	public static void main(String args[]) throws IOException
	{
		File f=new File(args[0]);
		
		cuenta c=new cuenta();
		System.out.println("El fichero tiene "+ c.cuentaPalabras(f) + " palabras.");
	}
}
	
