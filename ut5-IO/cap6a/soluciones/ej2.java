/*2. Programa que se ejecute de igual forma que el anterior, con un único número de línea, y borre esa línea del fichero indicado.*/
import java.io.*;
public class ej2
{
	public static void main(String[] args)
	{
		String s;
		int cont = 0;
		File f = null;
		File fin = null;
		FileReader fr = null;
		FileWriter fw = null;
		if(args.length < 2)
		{
			System.out.println("Debes especificar el nombre del fichero y el número de la línea que quieres borrar");
			System.exit(0);
		}

		f = new File(args[0]);
		fin = new File("volcado.txt");
		try
		{
			fr = new FileReader(f);
			fw = new FileWriter(fin);
		}
		catch(FileNotFoundException FNE){ System.err.println("Se ha liado"); }
		catch(IOException er){ System.err.println("Se ha liado 2"); }
		BufferedReader br = new BufferedReader(fr);

		try
		{
			while((s = br.readLine()) != null)
			{
				cont++;
				if(cont == Integer.parseInt(args[1]))
					continue;
				else
					fw.write(s + "\n");
			}
            if (fw != null)
				fw.close();
            if (br != null)
                br.close();
		}
		catch(IOException e)
		{
			System.err.println("IOException");
		}
        f.delete();
        fin.renameTo(f);
	}
}
