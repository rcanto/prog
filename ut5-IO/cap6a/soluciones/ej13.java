/* 13. Haciendo uso de la clase RandomAccessFile realiza un programa que acepte pares de números ( int y double), de tal forma que el programa escriba en binario a fichero el valor double en la posición indicada por el entero. El programa pedirá pares de números hasta acabar con un par en el que el entero valga cero.

	Ejemplo:
	
	2 3.6
	5 11.5
	3 -3.1
	0 9.4
	
, generará el siguiente fichero:

	--------------------------
	|    | 3.6|-3.1|    |11.5|
	-------------------------- */
import java.util.Scanner;
import java.io.*;

public class ej13
{
	public static void main(String[] args) {
		Scanner ent = new Scanner(System.in);
		try
		{
			RandomAccessFile raf = new RandomAccessFile("nums.dat","rw");
			System.out.println("Introduce un par de números: (entero + real) (0 entero para acabar)");
			int pos = ent.nextInt();
			double valor = ent.nextDouble();
			while ( pos != 0)
			{
				if (pos > 0)
				{
						// 8 es el tamaño de cada double
						raf.seek(8*(pos-1));
						raf.writeDouble(valor);
				}
				System.out.println("Introduce un par de números: (entero + real) (0 entero para acabar)");
				pos = ent.nextInt();
				valor = ent.nextDouble();	
			}
			if (raf != null) raf.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("El fichero no existe");
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}