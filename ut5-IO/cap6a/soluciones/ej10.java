/*
Programa que pida al usuario números double hasta acabar con un 0 
Los escriba a fichero y mostrándolo por pantalla los números, El promedio y cuántos han sido.
*/
import java.io.*;
public class ej10
{
	public static void main(String[] args)
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double num;
		try
		{
			FileOutputStream fos = new FileOutputStream("numeros2.data");
			DataOutputStream dos = new DataOutputStream(fos);
			System.out.println("Introduce un double (0 para acabar): ");
			num = Double.parseDouble(br.readLine());
			while (num != 0)
			{
				dos.writeDouble(num);
				System.out.println("Introduce un double (0 para acabar): ");
				num = Double.parseDouble(br.readLine());
			}

			if (dos != null) 
			{
				dos.close();
				fos.close();
			}

			System.out.println();

			File f = new File("numeros2.data");
			if (f.exists())
			{
				FileInputStream fis = new FileInputStream(f);
				DataInputStream dis = new DataInputStream(fis);
				//double num2;
				double promedio = 0;
				int contador = 0;
				try
				{
					while (true) 
					{
						num = dis.readDouble();
						System.out.println(num);
						promedio += num;
						contador++;
					}
				}
				catch (EOFException eof) //Final del fichero
		        {
		            System.out.println(" --------------------------");
		        }
				if (dis != null) 
				{
					dis.close();
					fis.close();
				}
				System.out.println("El promedio es " + promedio/contador);
				System.out.println("Has introducido " + contador);
			}
		}
		catch(IOException e)
		{
				System.out.println("Excepción");
		}
	}
}
