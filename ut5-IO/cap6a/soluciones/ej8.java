/* 8. Programa que, utilizando la clase PushbackReader, haga sustituciones sobre un fichero de texto de forma que, siempre que encuentre una mayúscula que no vaya precedida por un espacio, inserte el correspondiente espacio en blanco.

	Ejemplo:
	
		Hola.Te escribo para ...
		
			será sustituido por
			
		Hola. Te escribo para ... */
import java.io.*;

public class ej8
{
	public static void main(String[] args)
	{
			// flujo de lectura
		File f = new File("texto.txt");
		File f2=null;
		try
		{
			FileReader fr = new FileReader(f);
			PushbackReader pbr = new PushbackReader(fr);
				// flujo de escritura
			f2 = new File("texto.new");
			FileWriter fw = new FileWriter(f2);
			int c;	// el caracter a leer o escribir en cada pasada
			while ( (c = pbr.read()) != -1)
			{
				if (c != '.')
					fw.write(c);
				else 	// es un punto
				{
					int csig = pbr.read();
					fw.write('.');
					if (csig != ' ')
					{
						fw.write(' ');fw.write(csig);
					}
					else // el siguiente ya era un espacio en blanco
						pbr.unread(' ');
				}
			}
			if (pbr != null) { pbr.close(); fr.close();}
			if (fw != null) { fw.close();}
		}
		catch (FileNotFoundException e)
		{
			System.err.println("Fichero no encontrado");
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
		f.delete();
		f2.renameTo(f);
	}
}
