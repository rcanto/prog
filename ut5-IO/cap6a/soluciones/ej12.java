/* 12. Crea una clase Diccionario con 2 atributos para contener la misma palabra en 2 idiomas. Realiza un programa que serialice 10 objetos Diccionario a fichero. El programa acabará mostrando los objetos leídos desde fichero. */

import java.io.*;
import java.util.Scanner;

class Diccionario implements Serializable
{
	private String sp;
	private String en;

	public Diccionario() { sp="coche"; en="car"; }
	public Diccionario(String s,String e) { sp = s ; en = e; }
	public String e2s(String e) { return sp; }
	public String s2e(String s) { return en; }
	public String toString() { return "Español: " + sp + ", en inglés: " + en; }
}

public class ej12
{
	private static final int TAM = 3;
	private static Scanner ent = new Scanner(System.in);

	public static void main(String[] args)
	{
		String e="",s="";
		Diccionario d[] = new Diccionario[TAM];
		for (int i=0; i < TAM ; i++)
		{
			System.out.println("Palabra en español:");
			s = ent.nextLine();
			System.out.println("La misma palabra en inglés:");
			e = ent.nextLine();
			d[i] = new Diccionario(s,e);
		}
		serializa(d);
		deserializa();
	}

	public static void serializa(Diccionario d[])
	{
		try
		{
			FileOutputStream fos = new FileOutputStream("diccionario.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			for ( int i = 0 ; i < TAM ; i++)
				oos.writeObject(d[i]);
			if (oos != null)
			{
				oos.close(); fos.close();
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("El fichero no existe");		
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());	
		}
	}

	public static void deserializa()
	{
		Diccionario d = null;
		File f = new File("diccionario.dat");
		if (f.exists())
		{
			try
			{
				FileInputStream fis = new FileInputStream("diccionario.dat");
				ObjectInputStream ois = new ObjectInputStream(fis);
				while (true)
				{
					try
					{
						d = (Diccionario) ois.readObject();
					}
					catch (ClassNotFoundException e)
					{
						System.err.println(e.getMessage());	
					}
					catch (EOFException e)
					{
						//System.err.println(e.getMessage());	
						break;
					}
					System.out.println(d);
				}		
				if (ois != null)
				{
					ois.close(); fis.close();
				}
			}
			catch (FileNotFoundException e)
			{
				System.out.println("El fichero no existe");		
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());	
			}
		}
		else
			System.out.println("El fichero no existe");		
	}
}
