import java.util.Scanner;
import java.io.*;

public class ej12
{
	final static int tam=10; 
	public static void main(String[] args) throws IOException,ClassNotFoundException
	{
		Diccionario d[] = new Diccionario[10];
		int i; String s1="",s2="";
		Scanner ent=new Scanner(System.in);
		FileOutputStream fos=new FileOutputStream("diccionario.dat");
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		
		// escritura
		for(i=0;i<tam;i++)
		{
			System.out.println("Introduce una palabra en inglés:");
			s1=ent.nextLine();
			System.out.println("Introduce ahora la palabra que lo traduce al castellano:");
			s2=ent.nextLine();
			d[i] = new Diccionario(s2,s1);
			oos.writeObject(d[i]);
		}
		if (oos != null) { fos.close(); oos.close(); }
		// lectura
		File f=new File("diccionario.dat");
		if (f.exists())
		{
			FileInputStream fis=new FileInputStream(f);
			ObjectInputStream ois=new ObjectInputStream(fis);
			i=0;
			try
			{
				while (true)
				{
					d[i]=(Diccionario)ois.readObject();
					System.out.println(d[i].getSpanish() + " --> " + d[i].getEnglish());
				}
			}
			catch (EOFException e)
			{	System.out.println("==============================");
			}
			if (ois!=null) { fis.close(); ois.close(); }
		}	
	}
}
