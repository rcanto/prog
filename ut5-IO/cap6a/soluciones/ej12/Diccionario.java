import java.io.Serializable;

public class Diccionario implements Serializable
{
	private String spanish="";
	private String english="";
	
	public Diccionario(String s,String e) { spanish=s; english=e;}
	
	public String getSpanish() { return spanish; }
	public String getEnglish() { return english; }
}
