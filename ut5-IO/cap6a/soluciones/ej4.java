import java.io.*;
import java.util.StringTokenizer;

/* 4.  Realiza un programa que cuente las líneas, palabras y caracteres existentes en un fichero de texto pasado como parámetro. */

public class ej4
{
    public static void main(String args[])
	{
		if (args.length > 0 )
		{
			int contL = 0, contC = 0, contP = 0;
			String s="";
			File f = null;
			FileInputStream fis = null;
			InputStreamReader isr = null;
			BufferedReader br = null;
			
				f = new File(args[0]);
			try
			{
				try
				{
					fis = new FileInputStream(f);
					isr = new InputStreamReader(fis);
					br = new BufferedReader(isr);
				}
				catch (FileNotFoundException e)
				{
					System.out.println(e.getMessage());
					System.exit(0);	
				}
				
				try
				{
					s = br.readLine();
					while ( s != null )
					{
						StringTokenizer st = new StringTokenizer(s);
						contP += st.countTokens();
						contC += s.length();
						contL++;
						s = br.readLine();
					}
                    if (br != null) { br.close(); isr.close(); fis.close();}
				}
				catch (IOException e)
				{
					System.err.println(e.getMessage());
				}
			}
			catch (NullPointerException e)
			{
				System.err.println(e.getMessage());
			}
			
			System.out.println("Han sido " + contL + " líneas, " + contP + " palabras, y " + (contC + contL) + " caracteres.");
		}
		else
			System.out.println("Forma de uso: java ej4 fichero_de_texto");
			
			
	}
}
