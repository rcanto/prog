/* 5. Se necesita crear una clase censura con un método aplicaCensura, a la que se le pase como parámetro el nombre de un fichero, y sustituya cada aparición de "Linux" por "Unix" en dicho fichero. No te preocupes por los cambios de línea si se pierden. Recomendaciones: 

 - utiliza la clase Scanner, con sus métodos next y hasNext.
 - crea un segundo fichero en el que vayas escribiendo cada palabra, leída del fichero original, distinta de "Linux", y escribiendo "Unix" cuando la palabra leída sea aquélla. Al final, puedes eliminar el fichero original y renombrar el nuevo con el del original.
*/

 import java.io.*;

 class censura{
 	public void aplicaCensura(String n)
 	{
 		File fi = new File(n);
 		try
 		{
 			// flujo de lectura
	 		FileReader fr = new FileReader(n);
	 		BufferedReader br = new BufferedReader(fr);
	 		// flujo de escritura
	 		File fo = new File(n + ".new");
	 		FileWriter fw = new FileWriter(fo);

	 		String linea = br.readLine();
	 		while ( linea != null)
	 		{
	 			linea.replaceAll("Linux","Unix");
	 			fw.write(linea);
	 			linea = br.readLine();
	 		}
	 		if (br != null) { br.close(); fr.close();}
	 		if (fw != null) fw.close();
	 	}
 		fi.delete();
 		fo.renameTo(fi);
 	}
 }

 public class ej5
 {
 	public static void main(String[] args) {
 		if (args.length < 1)
 			System.out.println("Forma de uso: java ej5 /ruta/al/fichero");
 		else
 		{
 			censura c = new censura();
 			c.aplicaCensura(args[0]);
 		}
 	}

 }
