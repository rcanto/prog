import java.io.*;

public class ej11
{
	private static BufferedReader ent = new BufferedReader(new InputStreamReader(System.in));
	private static File f = new File("films.dat");
	private static FileInputStream fis = null;
	private static DataInputStream dis = null;
	public static void main(String[] args)
	{
		int opc=0,cod=0;
		while (true)
		{
			System.out.println("1. Alta de película\n2. Consulta de una película a partir de su código\n3. Listar todas las películas\n4. Borrar una película a partir de su código\n0. Salir del programa\n\tEscoge una opción:");
			try
			{
				opc = Integer.parseInt(ent.readLine());
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
			switch (opc)
			{
				case 1: alta();break;
				case 2: System.out.println("Introduce el código de la película:");
				try
				{
					cod = Integer.parseInt(ent.readLine());
				}
				catch (IOException e)
				{
					System.err.println(e.getMessage());
				}
				consulta(cod);break;
				case 3: lista();break;
				case 4: borra();break;
				case 0: System.exit(0);
				default: System.out.println("Opción incorrecta");
			}
		}
	}

	public static void alta()
	{
		String tit,dir; int cod;
		try
		{
			FileOutputStream fos = new FileOutputStream(f,true);
			// segundo parámetro a true para mantener el contenidor anterior del fichero
			DataOutputStream dos = new DataOutputStream(fos);
			System.out.println("Introduce el código de la película");
			cod = Integer.parseInt(ent.readLine());
			System.out.println("Introduce el título de la película");
			tit = ent.readLine();
			System.out.println("Introduce el director de la película");
			dir = ent.readLine();
			dos.writeInt(cod);
			dos.writeUTF(tit);
			dos.writeUTF(dir);
			if (dos != null) { dos.close(); fos.close();}
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}

	public static void lista()
	{
		String tit,dir; int cod;

		try
		{
			fis = new FileInputStream(f);
			dis = new DataInputStream(fis);
			System.out.println("----------------");
			try
			{
				while (true)
				{
					cod = dis.readInt();
					tit = dis.readUTF();
					dir = dis.readUTF();
					System.out.println(cod +" " + tit + "\t" + dir);
				}
			}
			catch (EOFException e)
			{
				System.out.println("----------------");
			}
			if (dis != null)
			{	dis.close(); fis.close(); }
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
	public static void borra()
	{

	}
	public static void consulta(int cod)
	{
		String tit,dir; int codf=1;
		try
		{
			fis = new FileInputStream(f);
			dis = new DataInputStream(fis);
			System.out.println("----------------");
			try
			{
				while (true)
				{
					codf = dis.readInt();
					tit = dis.readUTF();
					dir = dis.readUTF();
					if (codf == cod)
						System.out.println(cod +" " + tit + "\t" + dir);
				}
			}
			catch (EOFException e)
			{
				System.out.println("----------------");
			}
			if (dis != null)
			{	dis.close(); fis.close(); }
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}