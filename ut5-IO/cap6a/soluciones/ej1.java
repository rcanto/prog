/*1. Programa que muestre unas determinadas líneas de un fichero de texto. Tanto el fichero como los números de línea (en orden creciente) serán pasados como parámetros en la ejecución. Ejemplo:

	java ej1 fichero.txt 3 7 10*/
import java.io.*;
public class ej1
{
	public static boolean encontrado(int n,int[] a)
	{
		for(int i = 0 ; i < a.length ; i++)
			if (a[i] == n)
				return true;
		return false;
	}
	public static void main(String[] args)
	{
		String s;
		int cont = 1;
		File f = null;
		FileReader fr = null;
		if(args.length < 2)
		{
			System.out.println("Debes especificar el nombre del fichero y el número de las líneas que quieres ver");
			System.exit(0);
		}

		int n[] = new int[args.length-1];
		f = new File(args[0]);
		try
		{
			fr = new FileReader(f);
		}
		catch(FileNotFoundException FNE)
		{
			System.out.println("Se ha liado parda");
		}
		BufferedReader br = new BufferedReader(fr);

		for(int i = 0 ; i < args.length-1 ; i++)
		{
			n[i] = Integer.parseInt(args[i+1]);
		}

		try
		{
			while((s = br.readLine()) != null)
			{
				if(encontrado(cont,n))
				    System.out.println("Linea " + cont + " " + s);
				cont++;
			}
            if (br != null) { br.close(); fr.close(); }
		}
		catch(IOException e)
		{
			System.out.println("IOException");
		}
	}
}
